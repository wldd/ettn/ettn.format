from lxml import etree

test_objects = {
    "ettn_generic-originator_signed.xsd": "ettn_generic-originator_signed.xml",
    "ettn_generic-transporter_signed_load.xsd": "ettn_generic-transporter_signed_load.xml",
    "ettn_generic-recipient_signed.xsd": "ettn_generic-recipient_signed.xml",
    "ettn_generic-transporter_signed_unload.xsd": "ettn_generic-transporter_signed_unload.xml"
}

for xsd in test_objects:
    xsd_file_name = xsd
    schema_root = etree.parse(xsd_file_name)
    schema = etree.XMLSchema(schema_root)

    xml_filename = test_objects[xsd]
    xml = etree.parse(xml_filename)

    if not schema.validate(xml):
        print ("Error during validation in {}".format(xsd))
        print(schema.error_log)
