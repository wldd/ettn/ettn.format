from lxml import etree
import sys

xsd_file_name = sys.argv[1]
xml_filename = sys.argv[2]

schema_root = etree.parse(xsd_file_name)
schema = etree.XMLSchema(schema_root)

xml = etree.parse(xml_filename)

if not schema.validate(xml):
    print ("Error during validation in {}".format(xml_filename))
    print(schema.error_log)